@extends('admin._layouts.default') 

@section('col-rht')
<div class="col-rht-container">
    <div class="sec-hd">歡迎</div>
    <div class="sec-list">
        <ul>
            <li><a href="{{action('admin\UserController@getIndex')}}">瀏覽 用戶</a></li>
            <li><a href="{{action('admin\RoleController@getIndex')}}">瀏覽 用戶類型</a></li>
            <li><a href="">瀏覽 順箱庫存</a></li>
            <li><a href="">瀏覽 順箱歷史記錄</a></li>
            <li><a href="">瀏覽 客戶</a></li>
            <li><a href="">瀏覽 順箱</a></li>
            <li><a href="">瀏覽 基本設定</a></li>
            <li><a href="">瀏覽 管理假期</a></li>
        </ul>
    </div>
</div>
@endsection
