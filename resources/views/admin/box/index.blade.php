@extends('admin._layouts.default')

@section('col-rht')
<form method="get" id="myform" name="myform">
<div class="col-rht-container">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="sec-hd">順箱操作 &gt; 列表</div>
    <div class="item-list">
        <div id="node-menu">
            <div id="search-bar">
                <span>關鍵字: </span><input id="keyword" type="text" name="keyword" value="{{$keyword or ""}}"> 
                <button id="btnSearch" name="search_keyword"  type="submit" value="1">搜尋</button>
                <a href="{{action($controller.'@getIndex')}}" id="clearSearch">重設搜尋</a>
            </div>
            <div id="search-bar" class="clear" style="margin-top: 5px">
                <span>開始日期: <input id="from_date" type="text" name="from_date" value="{{$from_date or $now_date}}" class="datepicker"></span>
                <span>結束日期: <input id="to_date" type="text" name="to_date" value="{{$to_date or $now_date}}" class="datepicker"></span>
                <button name="search_date" type="submit" value="1" id="clearSearch">搜尋</button>
            </div>
        </div>
        <div id="tbl-tool">
            <div class="btns">
            <a onclick="checkAll('myform', 'del');" href="javascript:void(0);" >全選</a>
            <a onclick="checkInverse('myform', 'del');" href="javascript:void(0);" >選擇相反</a>
            </div>
            <div class="paging"></div>
        </div>
        <table width="100%" border="0" cellspacing="1" cellpadding="0" id="tbl-list" data-page-size="20">
            <thead>
                <tr>
                    @foreach ($fields as $item)
                    <th scope="col"><span>{{$item['display']}}</span></th>
                    @endforeach
                    <th scope="col"><span>完成鍵</span></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($result as $key => $item)
                <tr>
                    @foreach ($fields as $field => $row)
                    @if(isset($row['enum']))
                    <td>{{ $row['enum'][$item->{$field}] or $item->{$field} }}</td>
                    @else
                    <td><a href="#" class="{{ $field }}" data-type="text" data-pk="{{ $item->invoice_id }}" data-url="/post" data-title="Enter {{$row['display']}}">{{ $item->{$field} }}</a></td>
                    @endif
                    @endforeach
                    <td class="btns">
                        <a href="{{action($controller.'@getAction', [$item->box_id, $item->box_user_id, $item->invoice_id, 'complete'])}}">完成鍵</a>
                        <a href="{{action($controller.'@getAction', [$item->box_id, $item->box_user_id, $item->invoice_id, 'cancel'])}}">取消鍵</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="100">
                        <div class="pagination pagination-centered hide-if-no-paging"></div>
                    </td>
                </tr>
            </tfoot>
        </table>
        <div id="tbl-tool">
            <div class="btns">
            <a onclick="checkAll('myform', 'del');" href="javascript:void(0);"><span>全選</span></a>
            <a onclick="checkInverse('myform', 'del');" href="javascript:void(0);" ><span>選擇相反</span></a>
            </div>
            <div class="paging"></div>
        </div>
    </div>
</div>
</form>
@endsection

@section('page_js')
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jqueryui-editable/css/jqueryui-editable.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jqueryui-editable/js/jqueryui-editable.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd"
    });

    $.fn.editable.defaults.mode = 'popup';
    $('.location').editable();

    var $tbl = $('#tbl-list');
    $tbl.footable();
    // editor = new $.fn.dataTable.Editor( {
    //     table: "#tbl-list",
    //     idSrc:  [box_id, box_user_id, invoice_id],
    //     ajax: "{{action('admin\BoxController@postEdit')}}",
    //     fields: [{
    //             label: "庫位:",
    //             name: "location"
    //         }, {
    //             label: "庫存狀態:",
    //             name: "activity_status",
    //             type: "select",
    //             options: [
    //                 {
    //                     "value": "delivery_in_progress",
    //                     "label": "訂單處理中"
    //                 }, {
    //                     "value": "shipping_to_customer",
    //                     "label": "上門派箱中"
    //                 }, {
    //                     "value": "box_with_customer",
    //                     "label": "客戶處理中"
    //                 }, {
    //                     "value": "shipping_to_warehouse",
    //                     "label": "上門收箱中"
    //                 }, {
    //                     "value": "box_in_warehouse",
    //                     "label": "倉儲中"
    //                 }, {
    //                     "value": "contract_end",
    //                     "label": "完約"
    //                 }, {
    //                     "value": "cancel_order",
    //                     "label": "取消"
    //                 },
    //             ]
    //         }
    //     ]
    // });

    // $tbl.on( 'click', 'tbody td:nth-child(21n+7)', function (e) {
    //     editor.inline( this, {
    //         onBlur: 'submit'
    //     });
    // });

    // $tbl.on( 'click', 'tbody td:nth-child(21n+6)', function (e) {
    //     editor.inline(this);
    // });

    // $tbl.DataTable({
    //     searching: false,
    //     columnDefs: [{
    //             targets: [0, -1],
    //             orderable: false,
    //             searchable: false
    //         },{
    //             className: "width_80", targets: [7,8] 
    //         },{
    //             visible: false, targets: [0] 
    //         }
    //     ],
    //     columns: [
    //         { data: 'invoice_id' },
    //         @foreach ($fields as $field => $row)
    //         @if($field == 'activity_status')
    //         { data: '{{$field}}'},
    //         @else
    //         { data: '{{$field}}' },
    //         @endif
    //         @endforeach
    //         { data: 'complete' },
    //     ]
    // });
} );
</script>
@endsection