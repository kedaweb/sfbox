<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="robots" content="index,follow"/>
<meta http-equiv="content-language" content="zh-HK"/>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="viewport" content="width=device-width, user-scalable=no,initial-scale=1.0, maximum-scale=1.0" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<title>SF Box @yield('title')</title>
<link href="{{url('/assets/libs/footable/css/footable.core.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assets/libs/footable/css/footable.metro.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('/assets/css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css" />
{{-- <link href="{{url('/assets/libs/datatables/media/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" /> --}}
<link href="{{url('assets/libs/sco.js/css/scojs.css')}}" rel="stylesheet">
<link href="{{url('/assets/css/cms.css')}}" rel="stylesheet" type="text/css" />
</head>
<body>
@section('top_header')
<div id="top-header">
    <div id="top-title"><h1><span id="btn-nav-menu">☰</span><a href="#" target="_blank">SF Box</a></h1></div>
    <div id="top-logged">登入為：<strong>admin</strong> [<a href="{{action('Auth\AuthController@getLogout')}}">登出</a>] </div>
</div>
@show

@section('main')
<div id="main-cntnr">
    <div id="col-rht">
        @yield('col-rht')
    </div>
    <div id="nav-menu">
        <div id="main-panel"><span><a href="{{action('admin\HomeController@getIndex')}}">管理平台</a><span class="drop-down">&#9660;</span></span></div>
        <ul id="main-panel-menu">
            <li><a href="{{action('admin\HomeController@getIndex')}}" class="selected">管理系統主頁</a></li>
            <li><a href="{{action('Auth\AuthController@getLogout')}}">登出</a></li>
            <li><a href="#">賬戶設定</a></li>
        </ul>
        <div class="nav-hd"></div>
        <div id="user"><span><a href="{{action('admin\UserController@getIndex')}}">用戶</a><span class="drop-down">&#9660;</span></span></div>
        <ul id="user-menu">
            <li><a href="{{action('admin\UserController@getIndex')}}" class="list">用戶列表</a></li>
        </ul>
        <div id="user_type"><span><a href="{{action('admin\RoleController@getIndex')}}">用戶類型</a><span class="drop-down">&#9660;</span></span></div>
        <ul id="user_type-menu">
            <li><a href="{{action('admin\RoleController@getIndex')}}" class="list">用戶類型列表</a></li>
        </ul>
        <div id="box"><span><a href="{{action('admin\BoxController@getIndex')}}">順箱操作</a><span class="drop-down">&#9660;</span></span></div>
        <ul id="box-menu">
            <li><a href="{{action('admin\BoxController@getIndex')}}" class="list">順箱操作列表</a></li>
        </ul>
        <div id="inventory"><span><a href="{{action('admin\BoxInventoryController@getIndex')}}">順箱庫存</a><span class="drop-down">&#9660;</span></span></div>
        <ul id="inventory-menu">
            <li><a href="{{action('admin\BoxInventoryController@getIndex')}}" class="list">順箱庫存列表</a></li>
        </ul>
        <div id="activity"><span><a href="#">順箱歷史記錄</a><span class="drop-down">&#9660;</span></span></div>
        <ul id="activity-menu">
            <li><a href="#" class="list">順箱歷史記錄列表</a></li>
        </ul>
        <div id="report"><span><a href="{{action('admin\CustomerController@getIndex')}}">客戶</a><span class="drop-down">&#9660;</span></span></div>
        <ul id="report-menu">
            <li><a href="{{action('admin\CustomerController@getIndex')}}" class="list">客戶列表</a></li>
        </ul>
        <div id="app_config"><span><a href="#">基本設定</a><span class="drop-down">&#9660;</span></span></div>
        <ul id="app_config-menu">
            <li><a href="/cms/app_config/list.php" class="list">修改基本設定</a></li>
        </ul>
        <div id="holiday"><span><a href="#">管理假期</a><span class="drop-down">&#9660;</span></span></div>
        <ul id="holiday-menu">
            <li><a href="/cms/holiday/list.php?keyword=" class="list">管理假期列表</a></li>
        </ul>
        <div id="sale-report"><span><a href="{{action('admin\SaleReportController@getIndex')}}">銷售報告</a></span></div>
        <div id="order-template"><span><a href="#">訂單模版</a></span></div>
    </div>
</div>
@show

@section('footer')
<div id="footer"><span>Version: 2.1</span></div>
@show

<script src="{{url('/assets/js/html5.js')}}"></script>
<script src="{{url('/assets/js/jquery.min.js')}}"></script>
<script src="{{url('/assets/js/jquery-migrate.min.js')}}"></script>
<script src="{{url('/assets/js/jquery.tools.min.js')}}"></script>
<script src="{{url('/assets/js/jquery-ui.min.js')}}"></script>
<script src="{{url('/assets/js/jquery.cookies.js')}}"></script>
{{-- <script src="{{url('/assets/libs/datatables/media/js/jquery.dataTables.min.js')}}"></script> --}}
{{-- <script src="{{url('/assets/libs/footable/dist/footable.all.min.js')}}"></script> --}}
<script src="{{url('/assets/libs/footable/js/footable.js')}}"></script>
<script src="{{url('/assets/libs/footable/js/footable.sort.js')}}"></script>
<script src="{{url('/assets/libs/footable/js/footable.paginate.js')}}"></script>
<script src="{{url('assets/libs/sco.js/js/sco.message.js')}}"></script>
<script type="text/javascript" src="{{url('/assets/js/list.js')}}"></script>
<script type="text/javascript" src="{{url('/assets/js/detail.js')}}"></script>
<script type="text/javascript">
$(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    @if(session()->has('message'))
    $.scojs_message("{!! session('message') !!}", $.scojs_message.TYPE_OK);
    @endif

    $("#col-rht").height(window.innerHeight-40);
    //$(".item-list").width($("#tbl-list").width());
    $(window).resize(function(e){
        $("#col-rht").height(window.innerHeight-40);
        //$(".item-list").width($("#tbl-list").width());
    });
    $('#user').addClass('selected');
    $('#user-menu').toggle();
    var navArray = ['main-panel','user','user_type','inventory','activity','report','box','app_config','holiday'];
    $.each( navArray, function(i, n){
        $(('#'+n)).click(function() {
              $(('#'+n+'-menu')).slideToggle('medium');
              if ($(this).is('.selected')) {
                  $(this).removeClass('selected');
                } else {
                  $(this).addClass('selected');
                }
        });
        $(('#'+n+' a')).click(function(event) {
          event.stopPropagation();
        })
        $(('#'+n+'.drop-down')).click(function(event) {
          event.stopPropagation();
          $(('#'+$(this).parent().parent().attr("id")+'-menu')).slideToggle('medium');
        })
    });
    $(('#btn-nav-menu')).click(function(event) {
      event.stopPropagation();
      if($("#nav-menu").width()<180)
          $("#nav-menu").stop(true,true).animate({'width':180});
      else
          $("#nav-menu").stop(true,true).animate({'width':0});
    });

    $("#btnSearch").button();
    $("#clearSearch").button();
    $("button").button();
    $("a",".btns").button();
    $("input:button","#tbl-list .btns").button();

    var current_url = '{{Request::path()}}';
    $('#nav-menu').find('ul').hide().each(function(index, element){
        var url = $(element).find('a').attr('href');
        if(url.indexOf(current_url) > 0){
            $(element).show();
        }
    });
})
</script>
<!--[if IE 6]><script type="text/javascript">$.ie6hover();</script><![endif]-->
@yield('page_js')
</body>
</html>
