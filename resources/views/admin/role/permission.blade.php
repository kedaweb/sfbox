@extends('admin._layouts.default')

@section('page_css')
<link href="/assets/switch/css/bootstrap-switch.min.css" rel="stylesheet">
@endsection

@section('title', '角色權限')

@section('col-rht')
<form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ $redirect_path }}/edit-permission">
<div class="col-rht-container">
    <div class="sec-hd">{{$row->display_name}} 權限  &gt; 列表</div>
    <div class="item-list">
        <div id="tbl-tool">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{ $row->id }}">
            <button type="submit">修改</button>
            <div class="btns">
            <a href="javascript:history.go(-1)">返回 列表</a>
            </div>
            <div class="paging"></div>
        </div>
        <table width="100%" border="0" cellspacing="1" cellpadding="0" id="tbl-list">
            <tbody>
                <tr>
                    <th scope="col" width="20" class="start"><span style="width:20px"></span></th>
                    <th scope="col"><span>權限</span></th>
                </tr>
                @foreach ($lists as $item)
                <tr>
                    <td>
                        @if ( in_array($item->id, $role_permissions) )
                        <input id="{{$item->name}}" name="{{$item->name}}" type="checkbox" value="{{$item->id}}" checked />
                        @else
                        <input id="{{$item->name}}" name="{{$item->name}}" type="checkbox" value="{{$item->id}}" />
                        @endif
                    </td>
                    <td><label for="{{$item->name}}"> {{ $item->display_name }} </label> </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</form>
@endsection

@section('page_js')
@include('component.sco_errormsg');
@endsection