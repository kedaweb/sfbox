@extends('admin._layouts.default')


@section('title', '帳戶-密碼修改')
@section('breadcrumb', '密碼修改')

@section('content')
<div class="jumbotron">
    <div class="bg">
        <form id="add_user" class="form-horizontal" method="post" action="/admin/users/reset-pwd">
            <div class="form-group">
                <label class="col-sm-3 control-label"><h4>密碼修改</h4></label>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-3 control-label">密碼</label>
                <div class="col-sm-4">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-3 control-label">確認密碼</label>
                <div class="col-sm-4">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-4">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <button type="submit" class="btn btn-default">修改</button>
                </div>
            </div>
        </form>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection