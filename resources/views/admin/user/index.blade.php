@extends('admin._layouts.default')

@section('col-rht')
<form method="post" id="myform" name="myform">
<div class="col-rht-container">
    <div class="sec-hd">用戶 &gt; 列表</div>
    <div class="item-list">
        <div class="toolbars" id="top-tools">
            <span class="btn-toggle"></span>
            <span class="tool-menu">
                <span><a class="btns" href="{{action($controller.'@getCreate')}}">新增</a></span>
            </span>
        </div>
        <div id="node-menu">
            <div id="search-bar"><span>關鍵字: </span>
                <input id="keyword" type="text" name="keyword" value="" onkeypress="if(event.keyCode==13) goSearch(this.form);" onfocus="this.select()"> 
                <a href="#" id="btnSearch" onclick="this.disabled=true;goSearch(document.forms['myform'])">搜尋</a> 
                <a href="list.php?keyword=&amp;date_from=&amp;date_to=&amp;job_role=" id="clearSearch" >重設搜尋</a>
            </div>
        </div>
        <div id="tbl-tool">
            <div class="btns">
            <a onclick="checkAll('myform', 'del');" href="javascript:void(0);" >全選</a>
            <a onclick="checkInverse('myform', 'del');" href="javascript:void(0);" >選擇相反</a>
            </div>
            <div class="paging"></div>
        </div>
        <table width="100%" border="0" cellspacing="1" cellpadding="0" id="tbl-list">
            <tbody>
                <tr>
                    <th scope="col" width="20" class="start"><span style="width:20px"></span></th>
                    <th scope="col" width="170"><span style="width:170px"></span></th>
                    <th scope="col"><span>登入帳號</span></th>
                    <th scope="col" width="120"><span style="width:120px"></a>帳戶類別</span></th>
                    <th scope="col" width="80"><span style="width:80px">用戶狀況</span></th>
                </tr>
                @foreach ($users as $key => $user)
                <tr>
                    <td>
                        <input type="checkbox" value="{{$user->id}}" name="del0">
                    </td>
                    <td class="btns"> 
                    <a href="{{action($controller.'@getEdit', [$user->id ])}}" title="修改/瀏覽" class="oi-pencil"><span><span class="oi" data-glyph="pencil"></span></span></a>
                    <a href="{{action($controller.'@getDelete', [$user->id ])}}" title="刪除" class="oi-delete"><span><span class="oi" data-glyph="delete"></span></span></a>
                    </td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->roles[0]->display_name }}</td>
                    <td><span style="cursor:pointer" onclick="goUpdateFlag(this,'2','status',1)"><span class="txt-true">{{ $user->is_ban ? "无效" : "有效" }}</span></span>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div id="tbl-tool">
            <div class="btns">
            <a onclick="checkAll('myform', 'del');" href="javascript:void(0);"><span>全選</span></a>
            <a onclick="checkInverse('myform', 'del');" href="javascript:void(0);" ><span>選擇相反</span></a>
            </div>
            <div class="paging"></div>
        </div>
    </div>
</div>
</form>
@endsection

@section('page_js')
<script type="text/javascript">
$(document).ready(function () {
    $("#field_display_form").css({'height':'auto'});
    $("#field_display_form").data('height',$("#field_display_form").height()+10);
    $("#field_display_form").css({'height':''});
    $("#field_display_form div").click(function(e){
        if($("#field_display_form").data('height')>$("#field_display_form").height())
            $("#field_display_form").animate({'height':$("#field_display_form").data('height')});
        else
            $("#field_display_form").animate({'height':'4.2em'});
    });
    if($.cookie('cms_user_hide_field')){
        var hide_field_arr = $.cookie('cms_user_hide_field').split(",");
        for( var key in hide_field_arr){
            $("#tbl-list tr").each(function(idx, obj){
                    $(this).children("th").eq(hide_field_arr[key]).hide();
                    $(this).children("td").eq(hide_field_arr[key]).hide();
            });
        }
    }
    $(".field_display_box").click(function(e){
        var field_cookie ="";
        $(".field_display_box:not(:checked)").each(function(idx, obj){
            field_cookie += (field_cookie)?","+$(this).val() : $(this).val();
        });
        $.cookie('cms_user_hide_field', field_cookie);
        var field_no = $(this).val();
        var field_checked = this.checked;
            $("#tbl-list tr").each(function(idx, obj){
                if(field_checked){
                    $(this).children("th").eq(field_no).show();
                    $(this).children("td").eq(field_no).show();
                }else{
                    $(this).children("th").eq(field_no).hide();
                    $(this).children("td").eq(field_no).hide();
                }
            });
    });
    
    $( "#top-tools .btn-toggle" ).toggle(
            function() { $("#top-tools .tool-menu").animate({width: "hide"})},
            function() {$("#top-tools .tool-menu").animate({width: "show"})}
    )
    })
</script>
@endsection