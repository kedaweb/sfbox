<?php

use Illuminate\Database\Seeder;

class EntrustTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function()
        {

            DB::table('users')->truncate();
            DB::table('roles')->truncate();
            DB::table('permissions')->truncate();

            DB::table('users')->insert(
                ['name' => 'admin', 'username' => 'admin','email' => 'admin@admin.com','password' => bcrypt('123456')]
            );


            DB::table('roles')->insert([
                ['name' => 'superadmin','display_name' => 'superadmin','description' => '超級管理員'],
                ['name' => 'user','display_name' => 'user','description' => '普通管理員'],
                ['name' => 'staff','display_name' => 'staff','description' => '員工']
            ]);

            $permissions = [
                ['name' => 'users-browse','display_name' => '用戶瀏覽','description' => '用戶瀏覽权限'],
                ['name' => 'users-add','display_name' => '用戶新增','description' => '用戶新增权限'],
                ['name' => 'users-edit','display_name' => '用戶修改','description' => '用戶修改权限'],
                ['name' => 'role-browse','display_name' => '用戶類型瀏覽','description' => '用戶類型瀏覽权限'],
                ['name' => 'role-add','display_name' => '用戶類型新增','description' => '用戶類型新增权限'],
                ['name' => 'role-edit','display_name' => '用戶類型修改','description' => '用戶類型修改权限'],
                ['name' => 'box-browse','display_name' => '顺箱操作瀏覽','description' => '顺箱操作瀏覽权限'],
                ['name' => 'box-edit','display_name' => '顺箱操作修改','description' => '顺箱操作修改权限'],
                ['name' => 'inventory-browse','display_name' => '順箱庫存瀏覽','description' => '順箱庫存瀏覽权限'],
                ['name' => 'inventory-edit','display_name' => '順箱庫存修改','description' => '順箱庫存修改权限'],
                ['name' => 'customer-browse','display_name' => '客户瀏覽','description' => '客户瀏覽权限'],
                ['name' => 'addbox-browse','display_name' => '新增顺箱瀏覽','description' => '新增顺箱瀏覽权限'],
                ['name' => 'addbox-add','display_name' => '新增顺箱新增','description' => '新增顺箱新增权限'],
                ['name' => 'addbox-edit','display_name' => '新增顺箱修改','description' => '新增顺箱修改权限'],
                ['name' => 'salereport-browse','display_name' => '銷售報告瀏覽','description' => '銷售報告瀏覽权限'],
                ['name' => 'ordertemplate-browse','display_name' => '訂單模版瀏覽','description' => '訂單模版权限'],
                ['name' => 'sysconfig.admin','display_name' => '基本设定','description' => '基本设定权限'],
                ['name' => 'holiday.admin','display_name' => '假期管理','description' => '假期管理权限'],
            ];
            DB::table('permissions')->insert($permissions);

            DB::table('role_user')->insert(['user_id' => '1','role_id' => '1']);

            for ($i=1; $i <= count($permissions); $i++) { 
                DB::table('permission_role')->insert(['permission_id' => $i,'role_id' => '1']);
            }
        });
    }
}