<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\BaseController;

use App\Model\Role;
use App\Model\Permission;

use Auth;
use Validator;
use Illuminate\Http\Request;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class RoleController extends BaseController
{
    use EntrustUserTrait;

    protected $namespace = 'admin';
    protected $fields = ['name', 'display_name', 'description'];
    protected $error;

    public function __construct()
    {
        parent::__construct();
        $this->redirect_path = action($this->controller.'@getIndex');
    }

    public function getIndex(Request $request)
    {
        $data['lists'] = Role::orderBy('id', 'asc')->get();

        $data['redirect_path'] = $this->redirect_path;
        return view('admin.role.index', $data);
    }

    public function getAdd()
    {
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.role.add', $data);
    }    

    public function postAdd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $data = $request->all();
            foreach ($this->fields as $key => $field) {
                $insert_data[$field] = isset($data[$field]) ? $data[$field] : "";
            }

            Role::create($insert_data);

            return redirect($this->redirect_path)->with('message', '新增成功!'.$this->error);
        }
    }    

    public function getEdit($id)
    {
        $data['row'] = Role::find($id);
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.role.edit', $data);
    }

    public function postEdit(Request $request)
    {
        $input_data = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            
            $row = Role::find($input_data['id']);

            foreach ($this->fields as $key => $field) {
                if( isset($input_data[$field]) ) {
                    $row->$field = $input_data[$field];
                }
            }

            $row->save();

            return redirect($this->redirect_path)->with('message', '修改成功!');
        } 
    }

    public function getDelete($id)
    {
        if($id == 1){
            return redirect($this->redirect_path)->with('message', '超级管理员类别不能删除!');
        }

        $role = Role::findOrFail($id); // Pull back a given role

        // Regular Delete
        $role->delete(); // This will work no matter what

        // Force Delete
        $role->users()->sync([]); // Delete relationship data
        $role->perms()->sync([]); // Delete relationship data

        $role->forceDelete();

        return redirect($this->redirect_path)->with('message', '刪除成功!');
    }

    public function postEditField(Request $request)
    {
        $input_data = $request->all();

        $row = Role::find($input_data['id']);
        $row->$input_data['field'] = $input_data['value'];
        $row->save();

        return response()->json(['info' => '修改成功']);
    }

    public function getEditPermission($id)
    {
        $data['row'] = Role::find($id);
        $data['role_permissions'] = $data['row']->perms->lists('id')->toArray();
        $data['lists'] = Permission::orderBy('id', 'asc')->get();
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.role.permission', $data);
    }

    public function postEditPermission(Request $request)
    {
        $role = Role::find($request->input('id'));
        $permissions = $request->except(['_token', 'id']);
        
        $role->detachPermissions($role->perms());
        $role->perms()->sync($permissions);

        return redirect($this->redirect_path)->with('message', '修改成功!');
    }
}
