<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\BaseController;

use App\User;
use App\Model\Role;

use Auth;
use Validator;
use Illuminate\Http\Request;

class HomeController extends BaseController
{

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->$request = $request;
    }

    public function getIndex()
    {
        return view('admin.index');
    }
}
