// JavaScript Document
function goDelete(thisForm, node_id) {
	if(confirm("Are you sure to delete all the selected data?")) {	
		var strQuery = "";
		if(getParameterByName("ref_id"))
			strQuery += "&ref_id="+getParameterByName("ref_id");
		if(getParameterByName("parent_id"))
			strQuery += "&parent_id="+getParameterByName("parent_id");
		thisForm.action = "godel.php?node_id="+node_id+strQuery;
		thisForm.submit();
	}
}

function goDuplicate(id, has_opt) {
	var has_opt = has_opt || false;
	if(has_opt){
			window.location = "duplicate.php?id="+id;
	}else{
		if(confirm("Are you sure to duplicate the selected data?")) {	
			window.location = "goduplicate.php?id="+id;
		}
	}
}

function goDeleteOne(thisForm, node_id, id, btn) {
	if(confirm("Are you sure to delete all the selected data?")) {			
		var strQuery = "";
		if(getParameterByName("ref_id"))
			strQuery += "&ref_id="+getParameterByName("ref_id");
		if(getParameterByName("parent_id"))
			strQuery += "&parent_id="+getParameterByName("parent_id");
		thisForm.action = "godel.php?node_id="+node_id+"&id="+id+strQuery;
		thisForm.submit();
	} else {
		btn.disabled = false;
	}
}

function goRate (slideId, field, score) {
	if(confirm("你要對這個評分嗎?")) {			
		$.post("updaterate.php", { id : slideId, key:"id", field:field, score:score },
			function(response){
				if(response){
				}else{
					alert("你的評分未能送出");
				}
			}, "json");
	}
}

function genCSV(thisForm, node_id) {
	if(confirm("Are you sure to generate the data into EXCEL format?")) {			
		thisForm.action = "gencsv.php?node_id="+node_id;
		thisForm.submit();
	}
}
function genXLSX(thisForm, node_id) {
	if(confirm("Are you sure to generate the data into EXCEL 2007 format?")) {			
		thisForm.action = "genxlsx.php?node_id="+node_id;
		thisForm.submit();
	}
}
function getCSV(thisForm, node_id) {
		node_id = (node_id)? node_id :"";
		thisForm.action = "getcsv.php?node_id="+node_id;
		thisForm.submit();
}
function goSearch(thisForm) {
		thisForm.submit();
}

function goEdit(slideId, node_id) {
	window.location = 'detail.php?node_id='+node_id+'&id='+slideId;
}

function goPhotos(slideId) {
	window.location = 'photolist.php?id='+slideId;
}

function goRef(slideId, refName, ref_id) {
	ref_id = ref_id || "";
	if(!ref_id)
	window.location = refName+'/list.php?ref_id='+slideId;
	else
	window.location = refName+'/list.php?ref_id='+slideId+'&parent_id='+ref_id;
}

function goNotify(slideId, fieldName) {
	if(confirm("是否要送出更新通知?")) {	
	$.ajax({
			url: "send_notify.php",
			dataType: 'json',
			type: "post",
			beforeSend: function() {
			},
			data: {
				id: slideId,
				keyField: fieldName
			},
			success: function(response) {
				if(response){
					alert("你的更新通知已被成功送出");
				}else{
					alert("你的更新通知未能送出");
				}
			}
	});
	}
}

function goRefEdit(slideId, refName) {
	window.location = refName+'detail.php?id='+slideId;
}

function goRefDelete(thisForm, node_id, refName) {
	if(confirm("Are you sure to delete all the selected data?")) {			
		thisForm.action = refName+"godel.php?node_id="+node_id;
		thisForm.submit();
	}
}

function goRefDeleteOne(thisForm, node_id, id, btn, refName) {
	if(confirm("Are you sure to delete all the selected data?")) {			
		thisForm.action = refName+"godel.php?node_id="+node_id+"&id="+id;
		thisForm.submit();
	} else {
		btn.disabled = false;
	}
}
function goInvent(slideId, secPath) {
	document.myOpener = window.open('inventory.php?section='+secPath+'&ref_id='+slideId, 'Inventory', 'width=850px, height=524px, scrollbars=1, resizable=1');
}

function goEnroll(slideId, section) {
	window.location = '../'+section+'/list.php?ref_id='+slideId;
}

function goDetails(slideId, node_id, section) {
	document.myOpener = window.open('order.php?section='+section+'&node_id='+node_id+'&id='+slideId, 'Order Details', 'width=850px, height=524px, scrollbars=1, resizable=1');
}

function goInvoice(slideId) {
	window.location = 'geninv.php?id='+slideId;
}

function pageChange(formName, page) {
	myForm = document.getElementById(formName);
	myForm.page_no.value = page;
	myForm.submit();
}

function checkAll(formName, leading) {
	var firstClick = "";
	var leadLen = leading.length;
	with(document.forms[formName]) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,leadLen) == leading) {
				firstClick = (firstClick == "") ? true : firstClick;
				thiselm.checked = firstClick;
			}
		}
	}
}

function checkInverse(formName, leading) {
	var leadLen = leading.length;
	with(document.forms[formName]) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.name.substring(0,leadLen) == leading) {
				thiselm.checked = !thiselm.checked;
			}
		}
	}	
}
function goQuestion(slideId, secPath) {
	window.location = '../'+secPath+'/list.php?ref_id='+slideId;
}
function goChart(slideId) {
	popWin('chart.php?ref_id='+slideId, 624, 600);
}
function popImg(path) {
	var windowprops ="top=50px,left=50px,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no";
	TheWin = window.open('','image',windowprops);
	TheWin.document.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html>');
	TheWin.document.write('<head><meta HTTP-EQUIV="imagetoolbar" CONTENT="no"><title>Enlarged Photo<\/title>');
	TheWin.document.write('<\/head><body style="overflow:auto;margin:0px;background-color:#ffffff;" onload="window.opener.popImgfitSize();">');
	TheWin.document.write('<img src="' + path + '" alt="" title="">');
	TheWin.document.write('<\/body><\/html>');
	TheWin.document.close();
}

function popWin(path, width, height) {
	var windowTop =document.documentElement.clientHeight+80;
	var windowLeft =document.documentElement.clientWidth;
	var windowprops ="top="+(windowTop-height)/2+"px,left="+(windowLeft-width)/2+"px,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes, width="+width+", height="+height+"";
	TheWin = window.open(path,'actionWindow',windowprops);
}

function popImgfitSize() {
	if (navigator.appName.indexOf("Microsoft")>=0) 
		TheWin.window.resizeBy(TheWin.document.images[0].width-TheWin.document.body.clientWidth, TheWin.document.images[0].height-TheWin.document.body.clientHeight); 
	else TheWin.window.resizeBy(TheWin.document.images[0].width-TheWin.window.innerWidth, TheWin.document.images[0].height-TheWin.window.innerHeight);
	TheWin.focus();
}
function getParameterByName(name){
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.search);
	if(results == null)
		return "";
	else
		return decodeURIComponent(results[1].replace(/\+/g, " "));
}
function goUpdateFlag (obj, slideId, field, isStatus) {
	if(confirm("是否確定要更新狀態?")) {
		$.post("updateflagstatus.php", { id : slideId, key:"id", field:field, isStatus:isStatus, text:$(obj).text() },
			function(data){
				$(obj).children().text(data.status);
				$(obj).children().removeAttr("class");
				$(obj).children().attr("class",data.classname);
			}, "json");
	}
}

function goAccept(thisForm, id, btn) {
	if(confirm("是否確定要接受工作?")) {			
		var strQuery = "";
		if(getParameterByName("ref_id"))
			strQuery += "&ref_id="+getParameterByName("ref_id");
		if(getParameterByName("parent_id"))
			strQuery += "&parent_id="+getParameterByName("parent_id");
		thisForm.action = "goconfirm.php?a=accept&id="+id+strQuery;
		thisForm.submit();
	} else {
		btn.disabled = false;
	}
}

function goConsider(thisForm, id, btn) {
	if(confirm("是否為接受工作考慮中?")) {			
		var strQuery = "";
		if(getParameterByName("ref_id"))
			strQuery += "&ref_id="+getParameterByName("ref_id");
		if(getParameterByName("parent_id"))
			strQuery += "&parent_id="+getParameterByName("parent_id");
		thisForm.action = "goconfirm.php?a=consider&id="+id+strQuery;
		thisForm.submit();
	} else {
		btn.disabled = false;
	}
}

function goReject(thisForm, id, btn) {
		var strQuery = "";
		if(getParameterByName("ref_id"))
			strQuery += "&ref_id="+getParameterByName("ref_id");
		if(getParameterByName("parent_id"))
			strQuery += "&parent_id="+getParameterByName("parent_id");
		thisForm.action = "goconfirm.php?a=reject&id="+id+strQuery;
		thisForm.submit();
}

function goConfirm(thisForm, id, btn) {
	if(confirm("是否確定確認工作分配?")) {			
		var strQuery = "";
		if(getParameterByName("ref_id"))
			strQuery += "&ref_id="+getParameterByName("ref_id");
		if(getParameterByName("parent_id"))
			strQuery += "&parent_id="+getParameterByName("parent_id");
		thisForm.action = "goconfirm.php?a=confirm&id="+id+strQuery;
		thisForm.submit();
	} else {
		btn.disabled = false;
	}
}

function toRejectList(id, field, merchant_id, branch_id, floor_id, btn) {
		$.post("torejectlist.php", { id : id, field : field, merchant_id : merchant_id, branch_id:branch_id, floor_id:floor_id },
			function(response){
				if(response){
						$(btn).unbind("click").click(function(e){
							e.preventDefault();
						});
					alert("成功加入拒絕工作名單");
				}else{
					if(status){
					alert("帳號未能加入拒絕工作名單");
					}else{
					alert("帳號未能從拒絕工作名單剔除");
					}
				}
			}, "json");
}
function goUserList(id, level) {
	var level = level || '';
		window.open("user_list.php?id="+id+"&level="+level);
}

function goPwd(slideId) {
	//window.open('send_delivered.php?id='+slideId);
	if(confirm("是否要發出登入資訊電郵及重設密碼?")) {	
	$.ajax({
			url: "send_password.php",
			dataType: 'json',
			type: "post",
			beforeSend: function() {
			},
			data: {
				id: slideId
			},
			success: function(response) {
				if(response){
					alert("登入資訊電郵已被成功送出");
				}else{
					alert("登入資訊電郵未能送出");
				}
			}
	});
	}
}
