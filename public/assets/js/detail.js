// JavaScript Document

function checkdatetime(input) {
	var validformat=/^\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}$/; //Basic check for format validity
	var returnval=false;
	if(input.value != "") {
		if (!validformat.test(input.value))
			alert("Invalid Date Format. Please correct and submit again.");
		else{ //Detailed check for valid date ranges
			var dateArr = input.value.split(" ")[0];
			var yearfield=dateArr.split("-")[0];
			var monthfield=dateArr.split("-")[1];
			var dayfield=dateArr.split("-")[2];
			var dayobj = new Date(yearfield, monthfield-1, dayfield);
			if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
				alert("Invalid Day, Month, or Year range detected. Please correct and submit again.");
			else
				returnval=true;
		}
		return returnval;
	} else {
		return true;
	}
}
		 
function checkdate(input) {
	var validformat=/^\d{4}\-\d{2}\-\d{2} \w{3}$/; //Basic check for format validity
	var returnval=false;
	if(input.value != "") {
		if (!validformat.test(input.value))
			alert("Invalid Date Format. Please correct and submit again.");
		else{ //Detailed check for valid date ranges
			var yearfield=input.value.split("-")[0];
			var monthfield=input.value.split("-")[1];
			var lastfield = input.value.split("-")[2];
			var dayfield= lastfield.split(" ")[0];
			var dayobj = new Date(yearfield, monthfield-1, dayfield);
			if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
				alert("Invalid Day, Month, or Year range detected. Please correct and submit again.");
			else
				returnval=true;
		}
		return returnval;
	} else {
		return true;
	}
}

function getFckEditorTextLength(fckName) {
	// This functions shows that you can interact directly with the editor area
	// DOM. In this way you have the freedom to do anything you want with it.
	
	// Get the editor instance that we want to interact with.
	var oEditor = FCKeditorAPI.GetInstance(fckName) ;

	// Get the Editor Area DOM (Document object).
	var oDOM = oEditor.EditorDocument ;

	var iLength ;

	// The are two diffent ways to get the text (without HTML markups).
	// It is browser specific.

	if ( document.all ) { // If Internet Explorer.
		var innerText = oDOM.body.innerText;
		iLength = innerText.length ;
	} else {// If Gecko.
		var r = oDOM.createRange() ;
		r.selectNodeContents( oDOM.body ) ;
		iLength = r.toString().length ;
	}

	return iLength;
}

function setEditorValue( instanceName, text )
{  
  // Get the editor instance that we want to interact with.
  var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;
  
  // Set the editor contents.
  oEditor.SetHTML( text ) ;
} 

function getEditorValue( instanceName ) 
{  
  // Get the editor instance that we want to interact with.
  var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;
  
  // Get the editor contents as XHTML.
  return oEditor.GetXHTML( true ) ;  // "true" means you want it formatted.
}

function goUpdate(myForm, returnBack) {
	var returnBack = returnBack || false;
	var errorFlag = false;
	var errorMsg = "";
	if(errorFlag) {
		alert(errorMsg);
		myForm.btnUpdate.disabled = false;
		return false;
	} else {
		myForm.method = "post";
		myForm.action = "todb.php?return="+returnBack;
		$("#"+myForm.getAttribute("id")).submit();
		myForm.btnUpdate.disabled = false;
	}
}

function resetForm( myForm ) {
	myForm.reset();
	for (var i = 0; i < fcklist.length; i++) {
		setEditorValue(fcklist[i], document.getElementById(fcklist[i]).value);
	}
}

$(document).ready(function () {
	$("input:button").button();
})

function BrowseServer( ckBasePath, startupPath, functionData)
{
	// You can use the "CKFinder" class to render CKFinder in a page:
	var finder = new CKFinder();

	// The path for the installation of CKFinder (default = "/ckfinder/").
	finder.BasePath = ckBasePath;
	//Startup path in a form: "Type:/path/to/directory/"
	finder.StartupPath = startupPath;

	// Name of a function which is called when a file is selected in CKFinder.
	finder.SelectFunction = SetFileField;

	// Additional data to be passed to the selectActionFunction in a second argument.
	// We'll use this feature to pass the Id of a field that will be updated.
	finder.SelectFunctionData = functionData;

	// Launch CKFinder
	finder.Popup();
}

// This is a sample function which is called when a file is selected in CKFinder.
function SetFileField( fileUrl, data )
{
	document.getElementById( data["selectFunctionData"] ).value = fileUrl;
	
	//alert(data.toSource());
	document.getElementById( 'thumbnails' ).innerHTML =
			'<div class="thumb">' +
				'<img src="' + fileUrl + '" alt="'+data["fileUrl"]+'" width="80" align="left" hspace="5" />' +
			'</div>';

	document.getElementById( 'preview' ).style.display = "";
}

function BrowseServerMulti( ckBasePath, startupPath, functionData)
{
	// You can use the "CKFinder" class to render CKFinder in a page:
	var finder = new CKFinder();

	// The path for the installation of CKFinder (default = "/ckfinder/").
	finder.BasePath = ckBasePath;
	//Startup path in a form: "Type:/path/to/directory/"
	finder.StartupPath = startupPath;

	// Name of a function which is called when a file is selected in CKFinder.
	finder.SelectFunction = SetFileFieldsMulti;

	// Additional data to be passed to the selectActionFunction in a second argument.
	// We'll use this feature to pass the Id of a field that will be updated.
	finder.SelectFunctionData = functionData;

	// Launch CKFinder
	finder.Popup();
}

function SetFileFieldsMulti( fileUrl, data )
{
	var functionData = data["selectFunctionData"].split(':');
	document.getElementById( functionData[0]+functionData[1] ).value = fileUrl;
	//alert(data.toSource());
	document.getElementById( 'thumbnails'+functionData[1] ).innerHTML =
			'<div class="thumb">' +
				'<img src="' + fileUrl + '" alt="'+data["fileUrl"]+'" width="80" align="left" hspace="5" /> <input type="button" value="Delete" onclick="delCKFields( \''+functionData[1]+'\' );" class="chk-box" />' +
			'</div>';
			$('#preview'+functionData[1]+' .chk-box').button();


	document.getElementById( 'preview'+functionData[1] ).style.display = "";
}

function delCKFields (idx, key){
	if(key){
		$.post('godel_ckfinder.php', { ck_id: key },
	   function(data) {
		// alert("Data Loaded: " + data);
		$('#browse'+idx).remove();
		$('#filename'+idx).remove();
		$('#preview'+idx).remove();
	   });
	}else{
		$('#browse'+idx).remove();
		$('#filename'+idx).remove();
		$('#preview'+idx).remove();
	}
}

function addCKFields(fieldName,fckEditorPath, data){
	var functionData = data.split(':');
	var idx = functionData[1];
	idx++;
	$('#ckfinder-list .btn-add').remove();
	$('#ckfinder-list').append('<br/><input type="text" id="'+fieldName+idx+'" name="'+fieldName+'[]" value="" /><input type="button" id="browse'+idx+'" value="Browse" onclick="BrowseServerMulti( \''+fckEditorPath+'/ckfinder/\',\'Images:/\',\''+functionData[0]+':'+idx+'\' );" class="chk-box" /><input type="hidden" id="'+fieldName+'_id'+idx+'" name="'+fieldName+'_id[]" value="" /><div id="preview'+idx+'" style="display:none"><div id="thumbnails'+idx+'"></div></div><div class="btn-add"><a href=\"javascript:addCKFields(\''+fieldName+'\',\''+fckEditorPath+'\',\''+functionData[0]+':'+idx+'\');\">Add</a></div>');
	$('#ckfinder-list input:button').button();
	$('#ckfinder-list a').button();
}

function addAnsFields(fieldName, rank, tableName){
	$.post("get_ans.php",
		  {fieldName:fieldName, rank: parseInt(rank)+1, tableName:tableName},
		  function(data){
			  $('.ans-sortable').append(data);
			  $("#poll-ans .btn-add a").attr("href","javascript:addAnsFields('"+fieldName+"','"+(parseInt(rank)+1)+"','"+tableName+"');");
		 });
}

function toBlackLIst(id, field, merchant_id, branch_id, floor_id, status, btn) {
	status = !status;
	action_txt = (status)?"你要將此帳號加入黑名單嗎?":"你要將此帳號從黑名單剔除嗎?";
		$.post("toblacklist.php", { id : id, field : field, merchant_id : merchant_id, branch_id:branch_id, floor_id:floor_id, status:status },
			function(response){
				if(response){
						$(btn).text("已加入黑名單");
						$(btn).data("status",status);
						$(btn).unbind("click").click(function(e){
							e.preventDefault();
						});
				}else{
					if(status){
					alert("帳號未能加入黑名單");
					}else{
					alert("帳號未能從黑名單剔除");
					}
				}
			}, "json");
}
function toFirstChoice(user_id, post_id,btn) {
	action_txt = "你要將此帳號設定為商戶首選嗎?";
	if(confirm(action_txt)) {			
		$.post("tofirstchoice.php", { id : post_id, user_id:user_id},
			function(response){
				if(response){
					$(btn).addClass("selected");
				}else{
					alert("帳號未能設定為商戶首選");
				}
			}, "json");
	}
}